#!/bin/bash
now=$(date +%s)
let "diff = $1 * -60" 
pushd rrd > /dev/null
for file in ./*.rrd; do
    echo "last $1 minutes of data for $file"
    echo "================================="
    rrdtool fetch $file AVERAGE --start $diff 
    echo "================================="
    echo
done
popd > /dev/null

    

