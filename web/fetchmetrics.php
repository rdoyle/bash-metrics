<?php
    function isfloat($val, $_){
        return !is_nan($val);
    }
    
    function printdata($rrdname, $minutes){
        $result = rrd_fetch( "rrd/$rrdname.rrd", array( "AVERAGE", "--resolution", "60", "--start", "-" . $minutes . "m" ) );
        $output = array_filter($result["data"]["v"], "isfloat", ARRAY_FILTER_USE_BOTH);
        echo json_encode($output);
    }
    
    printdata($_GET["rrd"], $_GET["minutes"]);
?>
