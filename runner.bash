#!/bin/bash
mkdir -p rrd
pushd scripts > /dev/null
reindex=0
for file in ./*.bash; do
    
    # Gather info about the script, run it, and store result
    unit=$(egrep "^#" $file | egrep "unit" | cut -d: -f2 | xargs)
    now=$(date +%s)
    metricname=$(echo $file | tac | cut -d. -f2 | tac | cut -d/ -f2)
    result=$(bash $file)
    sha=$(echo "$metricname:::$unit" | sha1sum | cut -d\  -f1)

    # We will need to do a re-index due to a new metric or a mismatching sum
    if [ -f ../rrd/$metricname.sum ]; then
        if [ "$sha" != "$(cat ../rrd/$metricname.sum)" ]; then
            reindex=1
	fi
    else
        reindex=1
    fi
	
    # Start updating rrd's with results of this run
    pushd ../rrd > /dev/null
    if [ -f $metricname.rrd ]; then
        rrdtool update $metricname.rrd $now:$result
    else
        rrdtool create $metricname.rrd --start $now --step 60 DS:v:GAUGE:300:U:U RRA:AVERAGE:0.5:1:525960
    fi
    popd > /dev/null
done

if [ $reindex -gt 0 ]; then
    echo "Triggering a re-index due to script metadata change or new addition"
    rm ../rrd/index.json
    for file in ./*.bash; do
	# Gather minimal necessary info
        unit=$(egrep "^#" $file | egrep "unit" | cut -d: -f2 | xargs)
        metricname=$(echo $file | tac | cut -d. -f2 | tac | cut -d/ -f2)
        sha=$(echo "$metricname:::$unit" | sha1sum | cut -d\  -f1)
        
        # Re-dump .sum files and recreate index.json	
	pushd ../rrd > /dev/null
	echo "$sha" > $metricname.sum
        if [ -f index.json ]; then
            jq ". += [[\"$metricname\", \"$unit\"]]" index.json > i2.json
	    mv i2.json index.json
        else
            echo "[[\"$metricname\", \"$unit\"]]" > index.json
        fi
        popd > /dev/null
    done
fi
popd > /dev/null
