#!/bin/bash
#unit: Load Average 
uptime | egrep -o "load average.*$" | cut -d: -f2 | cut -d, -f1 | xargs
